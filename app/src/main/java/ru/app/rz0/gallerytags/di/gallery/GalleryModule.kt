package ru.app.rz0.gallerytags.di.gallery

import dagger.Module
import dagger.Provides
import ru.app.rz0.gallerytags.di.AppRxSchedulers
import ru.app.rz0.gallerytags.domain.gallery.GalleryInteractor
import ru.app.rz0.gallerytags.models.db.RoomDb
import ru.app.rz0.gallerytags.presentation.view.base.BaseActivity
import ru.app.rz0.gallerytags.repositories.gallery.GalleryRepositoryImpl

@Module
class GalleryModule(private val activity: BaseActivity) {
    @Provides
    fun providesBaseActivity(): BaseActivity = activity

    @Provides
    fun providesGalleryRepository(db : RoomDb, schedulers: AppRxSchedulers)
            = GalleryRepositoryImpl(db, schedulers)

    @Provides
    fun provideGalleryInteractor(activity: BaseActivity, repository: GalleryRepositoryImpl)
            = GalleryInteractor(activity, repository)
}