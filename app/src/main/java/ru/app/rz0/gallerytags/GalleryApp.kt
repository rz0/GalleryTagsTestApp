package ru.app.rz0.gallerytags

import android.app.Application
import ru.app.rz0.gallerytags.GalleryApp.Companion.instance
import ru.app.rz0.gallerytags.di.AppModule


class GalleryApp : Application() {
    companion object {
        lateinit var instance: GalleryApp
    }

    private var _appModule: AppModule? = null

    public val appModule: AppModule
        get() {
            if (_appModule == null)
                _appModule = AppModule(this)
            return _appModule ?: throw AssertionError("Set to null by another thread")
        }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}