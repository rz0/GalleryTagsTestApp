package ru.app.rz0.gallerytags.presentation.view.base

import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.annotation.StringRes
import android.support.v7.app.AppCompatActivity
import android.widget.Toast

abstract class BaseActivity : AppCompatActivity(), BaseView {
    companion object {
        const val TITLE_NOT_SET = -1
    }

    @LayoutRes
    abstract fun layoutRes(): Int

    abstract fun initViews()

    @StringRes
    protected open fun titleResId(): Int {
        return TITLE_NOT_SET
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutRes())
        updateToolbar()
        initViews()
    }

    private fun updateToolbar() {
        if(titleResId() == TITLE_NOT_SET) return

        supportActionBar?.setTitle(titleResId())
    }

    override fun showError(error: Int) {
        //TODO переделать на иньекцию
        Toast.makeText(this, error, Toast.LENGTH_LONG).show()
    }
}