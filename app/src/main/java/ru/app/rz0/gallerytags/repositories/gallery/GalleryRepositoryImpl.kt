package ru.app.rz0.gallerytags.repositories.gallery

import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.model.Image
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import ru.app.rz0.gallerytags.di.AppRxSchedulers
import ru.app.rz0.gallerytags.entity.picture.PictureItem
import ru.app.rz0.gallerytags.models.db.RoomDb
import ru.app.rz0.gallerytags.presentation.view.base.BaseActivity
import javax.inject.Inject


class GalleryRepositoryImpl @Inject constructor(
        private val db: RoomDb,
        private val schedulers: AppRxSchedulers) : GalleryRepository {

    override fun checkImage(image: Image): Single<PictureItem> {
        return db.pictureDao().findPicture(image.id)
                .subscribeOn(schedulers.io)
                .observeOn(schedulers.main)
                .onErrorReturn { getNewPicture(image) }
                .map { it }
    }

    private fun getNewPicture(image: Image) =
            PictureItem().apply {
                id = image.id
                name = image.name
                path = image.path
            }

    /**
     * Необходимо передавать activity, потому что получение галереи возможно только через
     * onActivityResult
     */
    override fun loadPicturesGallery(activity: BaseActivity) {
        ImagePicker.create(activity)
                .single()
                .start()
    }

    override fun loadPictures(): Maybe<List<PictureItem>> =
            db.pictureDao().getAll()
                    .subscribeOn(schedulers.io)
                    .observeOn(schedulers.main)

    override fun filterPictures(str: List<String>): Single<List<PictureItem>> =
            loadPictures()
                    .subscribeOn(schedulers.io)
                    .observeOn(schedulers.main)
                    .flattenAsObservable { it }
                    .filter {
                        if (str.isEmpty())
                            return@filter true
                        else
                            it.tags.containsAll(str)
                    }
                    .toList()

    override fun savePicture(picture: PictureItem): Single<Boolean> =
            Single.fromCallable { db.pictureDao().insert(picture) }
                    .subscribeOn(schedulers.io)
                    .observeOn(schedulers.main)
                    .map { true }
}