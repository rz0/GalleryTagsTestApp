package ru.app.rz0.gallerytags.di

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.app.rz0.gallerytags.models.db.RoomConst.DB_NAME
import ru.app.rz0.gallerytags.models.db.RoomDb
import javax.inject.Singleton

@Module
class AppModule(private val context: Application) {

    @Provides
    @Singleton
    fun providesContext(): Context = context

    @Provides
    @Singleton
    fun providesDatabase(context: Context): RoomDb =
            Room.databaseBuilder(context, RoomDb::class.java, DB_NAME)
                    .build()

    @Provides
    @Singleton
    fun providesRxSchedulers() = AppRxSchedulers(
            io = Schedulers.io(),
            main = AndroidSchedulers.mainThread()
    )

}