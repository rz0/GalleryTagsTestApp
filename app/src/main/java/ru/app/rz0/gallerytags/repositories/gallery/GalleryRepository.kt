package ru.app.rz0.gallerytags.repositories.gallery

import com.esafirm.imagepicker.model.Image
import io.reactivex.Maybe
import io.reactivex.Single
import ru.app.rz0.gallerytags.entity.picture.PictureItem
import ru.app.rz0.gallerytags.presentation.view.base.BaseActivity


interface GalleryRepository {
    fun loadPicturesGallery(activity: BaseActivity)

    fun loadPictures(): Maybe<List<PictureItem>>

    fun filterPictures(str : List<String>): Single<List<PictureItem>>

    fun checkImage(image: Image): Single<PictureItem>

    fun savePicture(picture: PictureItem): Single<Boolean>
}