package ru.app.rz0.gallerytags.presentation.presenter.gallery

import com.esafirm.imagepicker.model.Image
import ru.app.rz0.gallerytags.domain.gallery.GalleryInteractor
import ru.app.rz0.gallerytags.entity.picture.PictureItem
import ru.app.rz0.gallerytags.presentation.presenter.base.BasePresenter
import ru.app.rz0.gallerytags.presentation.view.gallery.GalleryView

class GalleryPresenter(var interactor: GalleryInteractor) : BasePresenter<GalleryView>() {

    fun getGalleryClick() = interactor.getPicturesGallery()

    fun loadAllPictures() {
        interactor.loadAllPictures()
                .doOnSuccess { view?.showPlaceholder(it.isEmpty()) }
                .doOnError { view?.showPlaceholder(true) }
                .subscribe({ view?.showPicturesDb(it) }, { it.printStackTrace() })
                .connect()
    }

    fun onFilterImagesStart(str: List<String>) {
        interactor.filterPictures(str)
                .subscribe({ view?.showPicturesDb(it) }, { it.printStackTrace() })
                .connect()
    }

    fun chooseImageClick(image: Image) {
        interactor.checkImage(image)
                .subscribe({ view?.onPictureLoaded(it) }, { it.printStackTrace() })
                .connect()
    }

    fun savePictureDbClick(picture: PictureItem) {
        interactor.savePicture(picture)
                .subscribe({ view?.onPictureSaved() }, { it.printStackTrace() })
                .connect()
    }

    fun pictureWasAdded() = loadAllPictures()
}