package ru.app.rz0.gallerytags.models.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import io.reactivex.Maybe
import io.reactivex.Single
import ru.app.rz0.gallerytags.entity.picture.PictureItem
import ru.app.rz0.gallerytags.models.db.RoomConst.PICTURE_TABLE

@Dao
interface PictureDao {
    @Query("SELECT * FROM $PICTURE_TABLE")
    fun getAll(): Maybe<List<PictureItem>>

    @Query("SELECT * FROM $PICTURE_TABLE WHERE id = :id LIMIT 1")
    fun findPicture(id: Long): Single<PictureItem?>

    @Insert(onConflict = REPLACE)
    fun insert(pictureItem: PictureItem)
}