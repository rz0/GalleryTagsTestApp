package ru.app.rz0.gallerytags.presentation.view.gallery

import android.support.v7.widget.RecyclerView
import android.view.View
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.gallery_view_holder.view.*
import ru.app.rz0.gallerytags.entity.picture.PictureItem
import java.io.File


class GalleryViewHolder constructor(itemView: View): RecyclerView.ViewHolder(itemView) {
    fun bind(picture: PictureItem) {

        Glide.with(itemView.context!!)
                .load(File(picture.path))
                .into(itemView.holder_image_view)

        itemView.holder_chips_view.removeAllViews()
        itemView.holder_chips_view.addChips(picture.tags?.toTypedArray())
    }
}
