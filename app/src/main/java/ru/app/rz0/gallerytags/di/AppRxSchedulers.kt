package ru.app.rz0.gallerytags.di

import io.reactivex.Scheduler

data class AppRxSchedulers(
        val io: Scheduler,
        val main: Scheduler
)