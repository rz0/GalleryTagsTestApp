package ru.app.rz0.gallerytags.presentation.view.tags

import android.os.Bundle
import android.support.v4.app.FragmentManager
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.dialog_tags.view.*
import ru.app.rz0.gallerytags.R
import ru.app.rz0.gallerytags.entity.picture.PictureItem
import ru.app.rz0.gallerytags.presentation.view.base.BaseAlertDialog
import ru.app.rz0.gallerytags.utils.StrUtils.getStrFromTags
import ru.app.rz0.gallerytags.utils.StrUtils.splitByDivider
import ru.app.rz0.gallerytags.utils.afterTextChanged
import java.io.File


class TagsDialog : BaseAlertDialog() {
    private val picture: PictureItem get() = arguments?.getSerializable(PICTURE_ITEM) as PictureItem
    private var pictureListener: TagsPictureListener? = null

    companion object {
        const val PICTURE_ITEM = "picture"

        fun show(fragmentManager: FragmentManager, picture: PictureItem, pictureListener: TagsPictureListener) {
            val dialog = TagsDialog()

            val args = Bundle()
            args.putSerializable(PICTURE_ITEM, picture)

            dialog.arguments = args
            dialog.pictureListener = pictureListener
            dialog.show(fragmentManager, TagsDialog::javaClass.name)
        }
    }

    override fun title(): Int = R.string.tags_dialog_title

    override fun view(): Int = R.layout.dialog_tags

    override fun initViews() {
        Glide.with(context!!)
                .load(File(picture.path))
                .into(containerView.tags_image_view)

        addChips(picture.tags)

        initTextView()
    }

    private fun initTextView() {
        val str = getStrFromTags(picture.tags)
        containerView.tags_edittext.setText(str)
        containerView.tags_edittext.afterTextChanged {
            val tags = splitByDivider(it)
            picture.tags = tags
            addChips(tags)
            positiveButton?.isEnabled = it.trim().isNotEmpty()
        }
        containerView.tags_edittext.setSelection(str?.length ?: 0)
    }

    private fun addChips(tags: List<String>?) {
        containerView.tags_chips_view.removeAllViews()
        if (tags == null || tags.isEmpty())
            return

        containerView.tags_chips_view.addChips(tags.toTypedArray())
    }


    override fun positiveButton(): Int = R.string.ok

    override fun negativeButton(): Int = R.string.cancel

    override fun positiveClick() {
        pictureListener?.onPictureSelected(picture)
        dismissAllowingStateLoss()
    }

    override fun negativeClick() = dismissAllowingStateLoss()
}