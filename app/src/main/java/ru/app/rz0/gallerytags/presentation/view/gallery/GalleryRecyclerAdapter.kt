package ru.app.rz0.gallerytags.presentation.view.gallery

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import ru.app.rz0.gallerytags.R
import ru.app.rz0.gallerytags.entity.picture.PictureItem


class GalleryRecyclerAdapter constructor(context: Context, private val pictures: List<PictureItem>)
    : RecyclerView.Adapter<GalleryViewHolder>() {
    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GalleryViewHolder {
        val view = inflater.inflate(R.layout.gallery_view_holder, parent, false)
        return GalleryViewHolder(view)
    }

    override fun getItemCount() = pictures.size

    override fun onBindViewHolder(holder: GalleryViewHolder, position: Int) {
        holder.bind(pictures[position])
    }
}