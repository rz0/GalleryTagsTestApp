package ru.app.rz0.gallerytags.domain.gallery

import com.esafirm.imagepicker.model.Image
import ru.app.rz0.gallerytags.entity.picture.PictureItem
import ru.app.rz0.gallerytags.presentation.view.base.BaseActivity
import ru.app.rz0.gallerytags.repositories.gallery.GalleryRepository
import javax.inject.Inject


class GalleryInteractor @Inject constructor(
        private val activity: BaseActivity,
        private val galleryRepository: GalleryRepository) {

    fun getPicturesGallery() = galleryRepository.loadPicturesGallery(activity)

    fun loadAllPictures() = galleryRepository.loadPictures()

    fun filterPictures(str : List<String>) = galleryRepository.filterPictures(str)

    fun checkImage(image: Image) = galleryRepository.checkImage(image)

    fun savePicture(picture: PictureItem) = galleryRepository.savePicture(picture)
}