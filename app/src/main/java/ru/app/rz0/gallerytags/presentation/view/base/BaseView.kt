package ru.app.rz0.gallerytags.presentation.view.base

import android.support.annotation.StringRes

interface BaseView {
    fun showError(@StringRes error: Int)
}