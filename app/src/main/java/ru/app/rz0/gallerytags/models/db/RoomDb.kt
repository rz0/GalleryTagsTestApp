package ru.app.rz0.gallerytags.models.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import ru.app.rz0.gallerytags.entity.picture.PictureItem

@Database(entities = [PictureItem::class], version = 1)
@TypeConverters(RoomConverters::class)
abstract class RoomDb : RoomDatabase() {
    abstract fun pictureDao(): PictureDao
}