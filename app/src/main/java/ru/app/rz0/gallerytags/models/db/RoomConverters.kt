package ru.app.rz0.gallerytags.models.db

import android.arch.persistence.room.TypeConverter
import ru.app.rz0.gallerytags.utils.StrUtils

class RoomConverters {
    @TypeConverter
    fun fromListTagsToStr(tags: List<String>): String = StrUtils.getStrFromTags(tags).toString()

    @TypeConverter
    fun fromStrToListTags(str: String): List<String> = StrUtils.splitByDivider(str)
}
