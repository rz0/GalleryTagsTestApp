package ru.app.rz0.gallerytags.presentation.view.gallery

import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout.VERTICAL
import android.widget.Toast
import com.esafirm.imagepicker.features.ImagePicker
import kotlinx.android.synthetic.main.activity_gallery.*
import ru.app.rz0.gallerytags.GalleryApp
import ru.app.rz0.gallerytags.R
import ru.app.rz0.gallerytags.di.gallery.DaggerGalleryComponent
import ru.app.rz0.gallerytags.di.gallery.GalleryComponent
import ru.app.rz0.gallerytags.di.gallery.GalleryModule
import ru.app.rz0.gallerytags.domain.gallery.GalleryInteractor
import ru.app.rz0.gallerytags.entity.picture.PictureItem
import ru.app.rz0.gallerytags.presentation.presenter.gallery.GalleryPresenter
import ru.app.rz0.gallerytags.presentation.view.base.BaseActivity
import ru.app.rz0.gallerytags.presentation.view.tags.TagsDialog
import ru.app.rz0.gallerytags.presentation.view.tags.TagsPictureListener
import ru.app.rz0.gallerytags.utils.StrUtils
import javax.inject.Inject

class GalleryActivity : BaseActivity(), GalleryView {

    @Inject
    lateinit var interactor: GalleryInteractor

    private lateinit var presenter : GalleryPresenter

    override fun initViews() {
        buildGalleryComponent()
        presenter.view = this
        presenter.loadAllPictures()

        gallery_placeholder_button.setOnClickListener {
            getPictureGallery()
        }

        gallery_recycler_view.layoutManager = LinearLayoutManager(this, VERTICAL, false)
    }

    override fun layoutRes() = R.layout.activity_gallery

    override fun titleResId() = R.string.gallery_title

    private fun buildGalleryComponent(): GalleryComponent {
        val component = DaggerGalleryComponent.builder()
                .galleryModule(GalleryModule(this))
                .appModule(GalleryApp.instance.appModule)
                .build()
        component.inject(this)
        presenter = GalleryPresenter(interactor)
        return component
    }

    override fun onPictureLoaded(picture: PictureItem) {
        TagsDialog.show(supportFragmentManager, picture, object : TagsPictureListener {
            override fun onPictureSelected(picture: PictureItem) {
                presenter.savePictureDbClick(picture)
            }
        })
    }

    override fun showPicturesDb(pictures: List<PictureItem>) {
        gallery_recycler_view.adapter = GalleryRecyclerAdapter(this, pictures)
    }

    override fun onPictureSaved() {
        Toast.makeText(this, R.string.picture_success_add, Toast.LENGTH_LONG).show()
        presenter.pictureWasAdded()
    }

    private fun getPictureGallery() {
        presenter.getGalleryClick()
    }

    override fun showPlaceholder(show: Boolean) {
        if (show) {
            gallery_placeholder_button.visibility = View.VISIBLE
            gallery_placeholder_textview.visibility = View.VISIBLE
        } else {
            gallery_placeholder_button.visibility = View.GONE
            gallery_placeholder_textview.visibility = View.GONE
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            presenter.chooseImageClick(ImagePicker.getFirstImageOrNull(data))
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.gallery_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        val searchItem = menu.findItem(R.id.search_picture_menu_item)
        val searchView = searchItem.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                presenter.onFilterImagesStart(StrUtils.splitByDivider(newText))
                Log.d("S", "SEARCH $newText")
                return true
            }
        })

        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.add_picture_menu_item -> getPictureGallery()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onStop() {
        presenter.onStop()
        super.onStop()
    }
}
