package ru.app.rz0.gallerytags.presentation.view.tags

import ru.app.rz0.gallerytags.entity.picture.PictureItem


interface TagsPictureListener {
    fun onPictureSelected(picture: PictureItem)
}