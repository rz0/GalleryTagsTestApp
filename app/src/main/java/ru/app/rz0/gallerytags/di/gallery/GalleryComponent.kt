package ru.app.rz0.gallerytags.di.gallery

import dagger.Component
import ru.app.rz0.gallerytags.di.AppModule
import ru.app.rz0.gallerytags.presentation.presenter.gallery.GalleryPresenter
import ru.app.rz0.gallerytags.presentation.view.gallery.GalleryActivity
import javax.inject.Singleton

@Singleton
@Component(modules = [GalleryModule::class, AppModule::class])
interface GalleryComponent {
    fun inject(activity : GalleryActivity)
}