package ru.app.rz0.gallerytags.presentation.view.base

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.annotation.StringRes
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import ru.app.rz0.gallerytags.R


abstract class BaseAlertDialog : DialogFragment() {
    protected lateinit var containerView: View
    protected var positiveButton: Button? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity!!, R.style.CustomAlertDialogStyle)
        with(builder) {
            title()?.let { setTitle(it) }

            val inflater = LayoutInflater.from(activity)
            view()?.let {
                containerView = inflater.inflate(it, null, false)
                setView(containerView)
            }

            if (positiveButton() != 0) {
                setPositiveButton(positiveButton(), null)
            }

            if (negativeButton() != 0) {
                setNegativeButton(negativeButton(), null)
            }

            if (neutralButton() != 0) {
                setNeutralButton(neutralButton(), null)
            }

        }
        return builder.create()
    }

    open fun initViews() {

    }

    override fun onResume() {
        if (positiveButton() != 0) {
            positiveButton = getButton(DialogInterface.BUTTON_POSITIVE)
            positiveButton?.setOnClickListener { positiveClick() }
        }

        if (negativeButton() != 0)
            getButton(DialogInterface.BUTTON_NEGATIVE).setOnClickListener { negativeClick() }

        if (neutralButton() != 0)
            getButton(DialogInterface.BUTTON_NEUTRAL).setOnClickListener { neutralClick() }

        initViews()
        super.onResume()
    }

    override fun show(manager: FragmentManager, tag: String) {
        try {
            val ft = manager.beginTransaction()
            ft.add(this, tag)
            ft.commitAllowingStateLoss()
        } catch (e: IllegalStateException) {
            Log.d("ABSDIALOGFRAG", "Exception", e)
        }
    }

    private fun getButton(buttonPositive: Int): Button {
        return (dialog as AlertDialog).getButton(buttonPositive)
    }

    override fun getView() = containerView

    @StringRes
    protected abstract fun title(): Int?

    @LayoutRes
    protected abstract fun view(): Int?

    @StringRes
    open fun positiveButton(): Int = 0

    open fun positiveClick() {}

    @StringRes
    open fun negativeButton(): Int = 0

    open fun negativeClick() {}

    @StringRes
    open fun neutralButton(): Int = 0

    open fun neutralClick() {}

}