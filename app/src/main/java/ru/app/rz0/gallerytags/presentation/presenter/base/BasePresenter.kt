package ru.app.rz0.gallerytags.presentation.presenter.base

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import ru.app.rz0.gallerytags.presentation.view.base.BaseView
import rx.subjects.PublishSubject


abstract class BasePresenter<View : BaseView> {
    var view : View? = null

    private var compositeDisposable = CompositeDisposable()

    fun onStop() {
        compositeDisposable.dispose()
    }

    protected fun Disposable.connect() {
        if(compositeDisposable.isDisposed)
            compositeDisposable = CompositeDisposable()

        compositeDisposable.add(this)
    }
}
