package ru.app.rz0.gallerytags.entity.picture

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import ru.app.rz0.gallerytags.models.db.RoomConst.PICTURE_TABLE
import java.io.Serializable
import java.util.*

@Entity(tableName = PICTURE_TABLE)
class PictureItem : Serializable {
    @PrimaryKey
    var id: Long? = 0

    var name: String? = null
    var path: String? = null
    var tags : List<String> = Collections.emptyList()
}