package ru.app.rz0.gallerytags.presentation.view.gallery

import ru.app.rz0.gallerytags.entity.picture.PictureItem
import ru.app.rz0.gallerytags.presentation.view.base.BaseView


interface GalleryView : BaseView{
    fun onPictureLoaded(picture: PictureItem)

    fun onPictureSaved()

    fun showPicturesDb(pictures : List<PictureItem>)

    fun showPlaceholder(show: Boolean)
}