package ru.app.rz0.gallerytags.models.db


object RoomConst {
    const val PICTURE_TABLE = "pictures"
    const val DB_NAME = "pictures_db.db"
}