package ru.app.rz0.gallerytags.utils

import java.util.*


object StrUtils {
    const val DIVIDER = ","

    fun getStrFromTags(tags: List<String>?): String? {
        return tags?.joinToString(DIVIDER)
    }

    fun splitByDivider(str : String) : List<String> {
        if(str.isEmpty())
            return Collections.emptyList()

        return str.split(DIVIDER)
    }
}