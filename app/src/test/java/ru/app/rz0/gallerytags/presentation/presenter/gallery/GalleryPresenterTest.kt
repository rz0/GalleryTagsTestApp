package ru.app.rz0.gallerytags.presentation.presenter.gallery

import io.reactivex.Maybe
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import ru.app.rz0.gallerytags.domain.gallery.GalleryInteractor
import ru.app.rz0.gallerytags.presentation.view.gallery.GalleryView
import java.util.*

class GalleryPresenterTest {
    @Mock
    lateinit var view : GalleryView

    @Mock
    private lateinit var interactor: GalleryInteractor

    private lateinit var presenter: GalleryPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        presenter = GalleryPresenter(interactor)
        presenter.view = view
    }

    @Test
    @Throws(Exception::class)
    fun nothingHappensInitially() {
        verifyZeroInteractions(interactor)
        verifyZeroInteractions(view)
    }

    @Test
    @Throws(Exception::class)
    fun requestAndShowSomePictures() {
        `when`(interactor.loadAllPictures())
                .thenReturn(Maybe.just(Collections.emptyList()))
        presenter.loadAllPictures()

        verify(interactor).loadAllPictures()
        verify(view).showPicturesDb(Collections.emptyList())
    }
}