package ru.app.rz0.gallerytags.domain.gallery

import io.reactivex.Maybe
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*
import ru.app.rz0.gallerytags.entity.picture.PictureItem
import ru.app.rz0.gallerytags.presentation.view.base.BaseActivity
import ru.app.rz0.gallerytags.repositories.gallery.GalleryRepository

class GalleryInteractorTest {
    private lateinit var interactor : GalleryInteractor
    private lateinit var repository: GalleryRepository
    private lateinit var activity: BaseActivity
    private lateinit var pictures : List<PictureItem>

    @Before
    fun setup() {
        repository = mock(GalleryRepository::class.java)
        activity = mock(BaseActivity::class.java)
        interactor = GalleryInteractor(activity, repository)

    }

    @Before
    fun initList() {
        val first = PictureItem()
        val second = PictureItem()
        pictures = mutableListOf(first, second)
    }

    @Test
    fun checkLoadPictures() {
        `when`(repository.loadPictures())
                .thenReturn(Maybe.just(pictures))

        val testObserver = interactor.loadAllPictures().test()
        testObserver.awaitTerminalEvent()

        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
        testObserver.assertValue { it.size == 2 }

        verify(repository).loadPictures()
    }
}